package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func nbTotalVotants() {
	filePath := "fichier.txt"
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Erreur lors de l'ouverture du fichier :", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	totalVotants := 0

	const votantColumnIndex = 10

	if scanner.Scan() {} 

	for scanner.Scan() {
		line := scanner.Text()
		columns := strings.Split(line, ";") 

		if len(columns) > votantColumnIndex {
			votants, err := strconv.Atoi(columns[votantColumnIndex])
			if err != nil {
				fmt.Println("Erreur lors de la conversion du nombre de votants :", err)
				continue
			}
			totalVotants += votants
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Erreur lors de la lecture du fichier :", err)
	}

	fmt.Printf("Nombre total de votants : %d\n", totalVotants)
}

func nbVotesParCandidat() {
	filePath := "fichier.txt"
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Erreur lors de l'ouverture du fichier :", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	votesByCandidate := make(map[string]int)

	const nameColumnStart = 23 
	const step = 7 

	if scanner.Scan() {} 

	for scanner.Scan() {
		line := scanner.Text()
		columns := strings.Split(line, ";")

		for i := nameColumnStart; i < len(columns); i += step {
			if i+2 < len(columns) { 
				candidateName := columns[i] + " " + columns[i+1]
				votes, err := strconv.Atoi(columns[i+2])
				if err != nil {
					fmt.Println("Erreur lors de la conversion du nombre de voix pour", candidateName, ":", err)
					continue
				}
				votesByCandidate[candidateName] += votes
			}
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Erreur lors de la lecture du fichier :", err)
	}

	for candidate, votes := range votesByCandidate {
		fmt.Printf("%s a reçu %d votes\n", candidate, votes)
	}

}

func nbVotesParCandidatParDepartements() {
	filePath := "fichier.txt"
	votesByDeptAndCandidate := make(map[string]map[string]int)

	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Erreur lors de l'ouverture du fichier :", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	const deptColumnIndex = 1 
	const nameColumnStart = 23
	const step = 7 

	if scanner.Scan() {} 

	for scanner.Scan() {
		line := scanner.Text()
		columns := strings.Split(line, ";")

		department := columns[deptColumnIndex]
		if _, exists := votesByDeptAndCandidate[department]; !exists {
			votesByDeptAndCandidate[department] = make(map[string]int)
		}

		for i := nameColumnStart; i < len(columns); i += step {
			if i+2 < len(columns) { 
				candidateName := columns[i] + " " + columns[i+1]
				votes, err := strconv.Atoi(columns[i+2])
				if err != nil {
					fmt.Println("Erreur lors de la conversion du nombre de voix pour", candidateName, ":", err)
					continue
				}
				votesByDeptAndCandidate[department][candidateName] += votes
			}
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Erreur lors de la lecture du fichier :", err)
	}

	for department, candidatesVotes := range votesByDeptAndCandidate {
		fmt.Printf("Département : %s\n", department)
		for candidate, votes := range candidatesVotes {
			fmt.Printf("    %s a reçu %d votes\n", candidate, votes)
		}
	}

}

func palmares() {
	type DeptVotants struct {
		Dept   string
		Votants int
	}
	filePath := "fichier.txt"
	votantsParDept := make(map[string]int)

	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Erreur lors de l'ouverture du fichier :", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	const deptColumnIndex = 1
	const votantColumnIndex = 10

	if scanner.Scan() {}

	for scanner.Scan() {
		line := scanner.Text()
		columns := strings.Split(line, ";")

		if len(columns) > votantColumnIndex {
			dept := columns[deptColumnIndex]
			votants, err := strconv.Atoi(columns[votantColumnIndex])
			if err != nil {
				fmt.Println("Erreur lors de la conversion du nombre de votants :", err)
				continue
			}
			votantsParDept[dept] += votants
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Erreur lors de la lecture du fichier :", err)
	}

	var listeDeptVotants []DeptVotants
	for dept, votants := range votantsParDept {
		listeDeptVotants = append(listeDeptVotants, DeptVotants{Dept: dept, Votants: votants})
	}

	sort.Slice(listeDeptVotants, func(i, j int) bool {
		return listeDeptVotants[i].Votants > listeDeptVotants[j].Votants
	})

	for _, dv := range listeDeptVotants {
		fmt.Printf("%s a %d votants\n", dv.Dept, dv.Votants)
	}

}

func main() {
	nbTotalVotants()
	//nbVotesParCandidat()
	//nbVotesParCandidatParDepartements()
	//palmares()
}
